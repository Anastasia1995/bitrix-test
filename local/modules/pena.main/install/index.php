<?php

use Bitrix\Main\ModuleManager;

class pena_main extends CModule
{
    function __construct()
    {
        $this->MODULE_ID = 'pena.main';
        $this->MODULE_VERSION = '2.0.0';
        $this->MODULE_VERSION_DATE = '2023-07-27 00:00:00';
        $this->MODULE_NAME = 'Pena Main';
        $this->MODULE_DESCRIPTION = 'vpene.ru';
        $this->PARTNER_NAME = 'Pena Production';
        $this->PARTNER_URI = 'https://vpene.ru';
    }

    function DoInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
    }

    function DoUninstall()
    {
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }
}