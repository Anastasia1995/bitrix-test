<?php
namespace Pena\Main\Highload;


class Vote extends Absctractclasses\HlAbstract
{
    public string $hlName = 'Vote';
    public string $tableName = 'pena_vote';

    /** Entity fields */
    public const POST_ID = 'UF_POST_ID';
    public const IP = 'UF_IP';
    public const DATE = 'UF_DATE';
}