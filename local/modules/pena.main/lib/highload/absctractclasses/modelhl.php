<?php

namespace Pena\Main\Highload\Absctractclasses;

interface ModelHl
{
    public const module = 'highloadblock';

    public function getElements(array $params);

    public function getClassHl();

    public function add(array $params);

    public function delete(int $id);

    public function update(int $id, array $params);
}
