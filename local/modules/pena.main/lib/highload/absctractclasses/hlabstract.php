<?php
namespace Pena\Main\Highload\Absctractclasses;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\DB\Result;
use Bitrix\Main\Entity\AddResult;
use Bitrix\Main\Entity\DeleteResult;
use Bitrix\Main\Entity\UpdateResult;
use Bitrix\Main\Loader;
use Bitrix\Highloadblock\HighloadBlockTable as HL;
use Bitrix\Main\LoaderException;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\SystemException;
use Exception;

abstract class HlAbstract implements ModelHl
{
    private $entity = null;
    private $dataClass = null;

    /**
     * @throws LoaderException
     */
    public function __construct()
    {
        if (Loader::includeModule(self::module)) {
            try {
                $oHl = HL::query()
                    ->where('NAME', $this->hlName)
                    ->setSelect(['*'])
                    ->setCacheTtl(846000)
                    ->exec()->fetch();
                $entity = HL::compileEntity($oHl);
                $this->entity = $entity;
                $this->dataClass = $entity->getDataClass();

            } catch (ObjectPropertyException|ArgumentException|SystemException $e) {
                throw new Exception($e->getMessage());
            }
        }
    }

    public function getEntity(): ?\Bitrix\Main\Entity\Base
    {
        return $this->entity;
    }

    public function getDataClass(): \Bitrix\Main\ORM\Data\DataManager|string|null
    {
        return $this->dataClass;
    }

    /**
     * @param $params
     * @return Result
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public function getElements($params): Result
    {
        return $this->getClassHl()::getList($params);
    }

    /**
     * @return string
     */
    public function getClassHl()
    {
        return $this->dataClass;
    }

    /**
     * Получить ID highload блока
     * */
    public function getHighloadId()
    {
        $highload = $this->getClassHl()::getHighloadBlock();
        return $highload['ID'];
    }

    /**
     * Получить список полей
     * @return array
     * */
    public function getFields(): array
    {
        $highload = $this->entity;
        return $highload->getFields();
    }

    /**
     * Получить массив значений поля типа Список
     * */
    public function getUserFieldListValues($ufCode)
    {
        $rs = \CUserFieldEnum::GetList(
            [],
            ['USER_FIELD_NAME' => $ufCode]
        );
        $arValues = [];
        while($arRes = $rs->GetNext()) {
            $arValues[] = $arRes;
        }
        return $arValues;
    }

    /**
     * @param array $params
     * @return AddResult
     * @throws Exception
     */
    public function add(array $params): AddResult
    {
        return $this->getClassHl()::add($params);
    }

    /**
     * @param int $id
     * @return DeleteResult
     * @throws Exception
     */
    public function delete(int $id): DeleteResult
    {
        return $this->getClassHl()::delete($id);
    }

    /**
     * @param int $id
     * @param array $params
     * @return UpdateResult
     * @throws Exception
     */
    public function update(int $id, array $params): UpdateResult
    {
        return $this->getClassHl()::update($id, $params);
    }
}
