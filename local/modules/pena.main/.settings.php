<?php

return [
    'controllers' => [
        'value' => [
            'namespaces' => [
                '\\Pena\\Main\\Controller' => 'api',
            ],
            'defaultNamespace' => '\\Pena\\Main\\Controller',
        ],
        'readonly' => true,
    ],
];