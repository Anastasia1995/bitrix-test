$(function () {
    $(document).on('click', '.post-item--vote-cnt:not(".active")', function (e) {
        let postId = $(e.target).attr('data-post-id');
        console.log(postId);
        let obPostData = {
            postId: postId,
        };
        BX.ajax.runComponentAction(
            'pena:post.list',
            'addVote',
            {
                mode: 'class',
                data: obPostData,
            }
        ).then(function (response) {
            console.log(response);
            if (response.status === 'success') {
                if (response.data && response.data.postId && response.data.voteCount) {
                    $('.post-item--vote-cnt[data-post-id="'+response.data.postId+'"]').addClass('active');
                    $('.post-item--vote-cnt[data-post-id="'+response.data.postId+'"]').attr('data-vote-cnt', response.data.voteCount);
                }
            }
        });
    })
});