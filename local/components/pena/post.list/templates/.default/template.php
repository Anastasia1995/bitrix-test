<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var array $APPLICATION */
/** @var array $component */

if (count($arResult['POST'])) {
    ?>
    <div class="post-container">
        <?php foreach ($arResult['POST'] as $post) { ?>
            <div class="post-item">
                <div class="post-item--name"><?= $post['UF_NAME']?></div>
                <div class="post-item--vote">
                    <span data-post-id="<?= $post['ID']?>"
                          data-vote-cnt="<?= $post['VOICE_COUNT']?>"
                          class="post-item--vote-cnt<?php if (!$post['USER_CAN_VOTE']) {?> active<?php }?>"
                    ></span>
                </div>
            </div>
        <?php } ?>
    </div>
    <?php
}
