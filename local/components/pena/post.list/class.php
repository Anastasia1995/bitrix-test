<?php

/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

use Bitrix\Main\Engine\{
    ActionFilter,
    Contract\Controllerable
};
use Bitrix\Main\Service\GeoIp\Manager;

/**
 * Class PostList - компонент вывода голосов за посты
 * */
class PostList extends CBitrixComponent implements Controllerable
{
    public const DEFAULT_CACHE_TIME = 360000000;

    public function __construct($component = null)
    {
        parent::__construct($component);

        $this->arResult['USER_IP_ADDRESS'] = Manager::getRealIp();
        $this->arResult['POST'] = [];
        $this->arResult['VOTE'] = [];
        try {
            $obPosts = new \Pena\Main\Highload\Post();
            $this->arResult['POST'] = $obPosts->getElements([
                'cache' => [
                    'ttl' => !empty($this->arParams['CACHE_TIME'])
                        ? $this->arParams['CACHE_TIME']
                        : self::DEFAULT_CACHE_TIME
                ]
            ])->fetchAll();

            $obVotes =  new \Pena\Main\Highload\Vote();
            $this->arResult['VOTE'] = $obVotes->getElements([
                'cache' => [
                    'ttl' => !empty($this->arParams['CACHE_TIME'])
                        ? $this->arParams['CACHE_TIME']
                        : self::DEFAULT_CACHE_TIME
                ]
            ])->fetchAll();

            foreach ($this->arResult['POST'] as &$post) {
                $postId = $post['ID'];
                $post['VOICE_COUNT'] = 0;
                $post['USER_CAN_VOTE'] = true;
                foreach ($this->arResult['VOTE'] as $vote) {
                    if ($vote['UF_POST_ID'] == $postId) {
                        if ($vote['UF_IP'] == $this->arResult['USER_IP_ADDRESS']) {
                            $post['USER_CAN_VOTE'] = false;
                        }
                        $post['VOICE_COUNT']++;
                    }
                }
            }
        } catch (\Exception) {}
    }

    /**
     * Подключение шаблона
     */
    public function executeComponent()
    {
        $this->includeComponentTemplate();
    }

    /**
     * Конфигуратор запросов
     * @return array
     */
    public function configureActions(): array
    {
        return [
            'addVote' => [
                'prefilters' => [
                    new ActionFilter\HttpMethod([
                        ActionFilter\HttpMethod::METHOD_POST
                    ]),
                ],
                'postfilters' => []
            ],
        ];
    }

    /**
     * Добавить голос за пост
     *
     * @param string $postId
     * @return array
     * */
    public static function addVoteAction(string $postId): array
    {
        $postId = (int)$postId;
        $ip = Manager::getRealIp();

        if (!self::isPostExists($postId)) {
            return [
                'error' => true,
                'message' => 'Поста с ID = ' . $postId . ' не существует'
            ];
        }

        if (self::isVoteExists($ip, $postId)) {
            return [
                'error' => true,
                'message' => 'Вы уже проголосовали'
            ];
        }

        $obVote = new \Pena\Main\Highload\Vote();
        $obVote->add([
            'fields' => [
                'UF_POST_ID' => $postId,
                'UF_IP' => $ip,
                'UF_DATE' => date('d.m.Y H:i:s'),
            ]
        ]);
        $postVotes = $obVote->getElements(['filter' => ['UF_POST_ID' => $postId]])->fetchAll();

        return [
            'error' => false,
            'postId' => $postId,
            'voteCount' => count($postVotes)
        ];
    }

    public static function isVoteExists($ip, $postId): bool
    {
        $obVotes =  new \Pena\Main\Highload\Vote();
        $existsVote = $obVotes->getElements([
            'filter' => [
                'UF_IP' => $ip,
                'UF_POST_ID' => $postId
            ]
        ])->fetch();
        return (bool)$existsVote;
    }

    public static function isPostExists($postId): bool
    {
        $obPosts = new \Pena\Main\Highload\Post();
        $post = $obPosts->getElements(['filter' => ['ID' => $postId]])->fetch();
        return (bool)$post;
    }
}